![Bild 1](media/1.jpeg)

**Hilti MQ41, konsol för rörupphängning, som BIM-objekt.**

# Allt fler produktspecifika egenskaper tidigt i modellerna

> ##### Digitaliseringen sker allt snabbare och allt mer omfattande och numera är ett stort antal produkter och byggsystem tillgängliga som 3D-illustrationer, 3D-objekt eller som BIM-objekt. Men vem bestämmer när och vilka specifika produkter som ska placeras i byggnadsinformationsmodellerna? Och vilka är riskerna med att i ett tidigt skede ha en modell som till övervägande del har produktspecifika egenskaper?

FÖR MATERIALTILLVERKARNA HAR DET BLIVIT ALLT VIKTIGARE att kunna erbjuda sina produkter i digitalt format för att därmed tidigt kunna användas i modellerna och förhoppningsvis därmed uppnå en starkare låsning till specifika produkter.
​	– Många tillverkare har insett att det är en fördel och nödvändighet
att underlätta den virtuella produktbestämningsprocessen med hjälp av just deras produkter och byggsystem, säger Rogier Jongeling på BIM Alliance, och han menar att fördelarna främst finns inom tre områden.
​	• Visualisering – det går att nästan exakt se hur produkten kommer att se ut i verkligheten.
​	• Integration – virtuella produkter möjliggör en mängd olika analyser genom integration av olika programvaror. Här blir informationen om det aktuella objektet mycket viktig och dess egenskaper måste vara exakt definierade.
​	• Automatisering – genom att beskriva rätt instanser av en produkt eller byggsystem i en modell kan exempelvis analys, logistik och förtillverkning i hög grad automatiseras.
​	– Utvecklingen idag går mycket fort men den har ändå bara börjat och många produkter är kvar i den icke objektorienterade världen. Men faktum kvarstår att allt fler produkter börjar dyka upp allt tidigare i modeller från arkitekter och konstruktörer vilket skapar en del frågor, säger Rogier Jongeling. 

Är det projektören som ska bedöma vilka produkter som ska användas och hur upphandlingar ska hanteras? Vilka egenskaper ska läggas på objekt och vilka får ligga på tillverkaren hemsida? Vad händer när man under förvaltningen byter ut en produkt till en produkt från en annan tillverkare? Ska alla produkter objektifieras?
​	En stor aktör inom området digitaliserade produkter är BIMobject i Malmö. Företaget erbjuder gratis tillgång till en stor mängd BIM-objekt från en mängd olika tillverkare och objekten kan laddas ner i de olika format som tillverkaren stödjer. Samtliga BIM-objekt finns i en molnbaserad produktkatalog som helt och hållet är baserad på verkliga produkter. Ett av de företag som valt att sluta avtal med BIMobject är Hilti Sverige.
​	– Vi vill som leverantör vara med så tidigt som möjligt i projekteringen och då måste vi förändra hur vi jobbar och säkerställa att vi adderar värde till processen, säger Matthias Bayer, BIM-ansvarig på Hilti Sverige. Vi har systematiskt arbetat mot konstruktörer och arkitekter under lång tid och har insett att vi måste följa utvecklingen för att vara aktuella inför framtida projekt. Vi utvecklar vår ingenjörskår till att kunna gå ut med konkret budskap till beställare, arkitekter och konstruktörer för att vi därmed ska kunna vara delaktiga i processen så tidigt som möjligt. Konstruktörerna är en mycket viktig målgrupp att nå.

– Nu försöker vi utröna hur projekteringssteget har utvecklats över tid med de nya digitala verktygen och vi försöker förstå det i detalj. Hur jobbar konstruktörer proaktivt och var hittar de inspiration till att välja en viss lösning? Vi vet att vi kan påverka genom besök och genom att föreläsa och informera om våra produkter, men vi vill även vara närvarande på de interaktiva plattformarna. 
​	Matthias Bayer ser det som viktigt att det för projektörer och konstruktörer finns så få avbrott som möjligt i själva arbetsprocessen och därför är det viktigt att enkelt tillhandahålla väldigt många kvalitativa objekt som gör arbetet smidigt.
​	– Det är vi som känner våra produkter bäst och vet vilket värde de kan skapa i byggprocessen. Om vi kan erbjuda både teknisk support, beräkningar och dimensioneringar under projekteringssteget, support och montageinstruktioner under själva utförandefasen när allting byggs och i efterhand erbjuda nödvändig dokumentation och insikt i hur man ska serva produkterna, då är vi någonting på spåren.
​	Han ser ingen fara med att specifika produkter tidigt kommer in i informationsmodellerna. Material och produkter ändras ständigt och byts ut och det blir inte svårare för att en produkt finns med tidigt i modellen. Konstruktörer garderar sig oftast med att ange att en likvärdig produkt kan användas och en viss form av handlingsfrihet finns. Allt detta hänger även samman med entreprenadform.
​	– Vi kan göra vårt bästa för att vara med i processen och försöka följa spelets regler. Ansvaret för att få BIM fullt integrerat i alla processer, från stora till små, vilar dock inte på materialtillverkarna men vi vill gärna bidra med det vi kan.

ROGER MELANDER, BYGGNADSKONSTRUKTÖR på WSP Byggprojektering i Skellefteå, har använt sig av Hiltis produkter i digitaliserade former. Han tycker att konceptet med korrekta modeller av tillverkarens produkter är mycket intressant. En korrekt representation minskar risken för fel. Om detta ska kunna användas fullt ut är det BIM-objekt, med korrekt namngivning och kompletterande parameterdata, som är mest intressant.
​	– Fördelarna med en modell som har produktspecifika egenskaper i förhållande till en neutral modell finns framförallt i möjligheterna till egenkontroll och beställningslistor till entreprenören. Nackdelar kan exempelvis vara att om ett byte av leverantör presenteras sent i projektet, innebär det normalt ett visst merarbete för att byta ut produkten. Och alltför detaljerade objekt, utan möjlighet att styra vad som ska visas, kan innebära en prestandaförlust i 3D-modellen. Att byta en produkt till en annan är normalt inget tekniskt problem om objektet är uppbyggt på ett korrekt sätt, säger Roger Melander och fortsätter:

– Ett tidigt val av produktspecifika egenskaper kan påverka men under projektering utgår vi normalt från ett fabrikat för våra beräkningar. Ändringar är en normal del av processen som vi är vana vid att hantera. Vi låser oss inte vid ett tidigt produktval

om ändringar visar att det inte är lämpligt senare i processen. 
​	Roger Melander ser inte någon risk att en hel lösning baseras på specifika produktval. Om detta sker är det för att det inte finns några alternativ i just det aktuella fallet, men detta händer
ytterst sällan. Det är projektören som, baserat på de förutsättningar som finns, bedömer vilka exakta produkter som ska användas. Det finns alltid möjlighet för entreprenören att föreslå alternativ och ifall egenskaperna uppfyller eller överstiger
det föreskrivna kan projektören godkänna ett utbyte efter samråd. 
​	Tidpunkten när det är rätt läge att ta med produktspecifika produkter i modellen varierar, men Roger Melander menar att normalt har projekteringen hunnit ganska långt när denna fråga börjar vara aktuell. Framförallt vill man ha dessa objekt i modellen innan ritningar upprättas. 

BIM ALLIANCE FÖLJER AKTIVT UTVECKLINGEN inom det här området, bland annat genom ett aktivt samarbete med material- och systemleverantörerna.
​	– Jag är övertygad om att det kommer att hända mycket inom digitalisering av produkter och att vi än så länge bara sett början av denna utveckling, säger Rogier Jongeling.
Mars 2015 														Göran Nilsson